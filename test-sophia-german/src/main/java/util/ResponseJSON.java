package util;

/**
 * Created by Sofiko on 06.04.15.
 */
public class ResponseJSON {

    private String status = "";
    private String errorMessage = "";

    public ResponseJSON(String status, String errorMessage) {
        this.status = status;
        this.errorMessage = errorMessage;
    }
}
