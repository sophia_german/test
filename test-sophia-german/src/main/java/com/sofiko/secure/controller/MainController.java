package com.sofiko.secure.controller;

import com.sofiko.secure.entity.Product;
import com.sofiko.secure.entity.Response;
import com.sofiko.secure.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import util.ProductJSON;
import util.ResponseJSON;



import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Controller
@RequestMapping("/")
public class MainController {

    @Autowired
    ProductService productService;

   @RequestMapping(method = RequestMethod.GET)
    public String start(Model model){
        return "index";
    }


    //Find products by name
    @RequestMapping(value = "/findproduct", method = RequestMethod.POST)
    public String findProduct(ModelMap model,@RequestParam String name) {
        model.addAttribute("products", productService.getProductByName(name));
        return "search";
    }


    // Find products by price between
    @RequestMapping(value = "/findProductByPrice", method = RequestMethod.POST)
    public String findProductByPrice(ModelMap model,@RequestParam int from, @RequestParam int to) {
        model.addAttribute("productsByPrice", productService.getProductByPrice(from, to));
        return "search";
    }

    // Get search page
    @RequestMapping(value = "/findproduct", method = RequestMethod.GET)
    public String findProducts(ModelMap model) {
        return "search";
    }


    // Display all products
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String product(ModelMap model) {
        model.addAttribute("products", productService.getProducts());
        return "products";
    }

    // Add product to db
    @RequestMapping(value = "/addproduct", method = RequestMethod.POST)
    public String createProduct(
            @RequestParam String name,
            @RequestParam int price
    ) {

        productService.createProduct(name, price);
        return "addproduct";
    }

    // Get addproduct page
    @RequestMapping(value = "/addproduct", method = RequestMethod.GET)
    public String addProducts(Locale locale, Model model) {
        return "addproduct";
    }



    // Get ajax page
    @RequestMapping(value = "/ajax", method = RequestMethod.GET)
    public String getAjaxPage(Locale locale, Model model) {
        return "ajax";
    }

    // Find products by name and return JSON
    @RequestMapping("/ajaxSearch")
    public @ResponseBody List<Product> performLooseSearch(@RequestParam("CHARS")String chars)
    {
        return productService.getProductByName(chars);
    }









}
