package com.sofiko.secure.service;

import com.sofiko.secure.entity.Product;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
@Transactional
public class ProductService {

    @Autowired
    SessionFactory sessionFactory;

    public void saveProduct(Product product) {
        sessionFactory.getCurrentSession().saveOrUpdate(product);
    }

    public Product getProduct(Integer id) {
        return (Product) sessionFactory.getCurrentSession().get(Product.class, id);
    }

    public List<Product> getProductByName(String name) {
        return sessionFactory.getCurrentSession()
                .createCriteria(Product.class)
                .add(Restrictions.eq("name", name))
                .list();
    }

    public List<Product> getProductByPrice(int from, int to) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Product where price between ? and ?")
                .setParameter(0, from)
                .setParameter(1, to)
                .list();

    }



    public List<Product> getProducts() {
        return sessionFactory.getCurrentSession()
                .createCriteria(Product.class)
                .list();
    }

    public void deleteProduct(Product product) {
        sessionFactory.getCurrentSession().delete(product);
    }


    public void createProduct(String name, int price) {
        Product product = new Product();

        product.setName(name);
        product.setPrice(price);

        saveProduct(product);
    }

}
