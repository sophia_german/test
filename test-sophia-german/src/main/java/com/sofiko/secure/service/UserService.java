package com.sofiko.secure.service;

import com.sofiko.secure.entity.User;

public interface UserService {

    User getUser(String login);

}
