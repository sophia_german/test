<%--
  Created by IntelliJ IDEA.
  User: Sofiko
  Date: 02.04.15
  Time: 22:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>ADD PRODUCT</title>

  <!-- Bootstrap core CSS -->
  <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">

  <%--<!-- Custom styles for this template -->--%>
  <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">


</head>

<body>

<div class="jumbotron" style="margin-top: 20px;">
  <h2>MENU</h2>

  <sec:authorize access="isAuthenticated()">

    <a  href="<c:url value="/products" />" role="button">Go To Products</a>
    <a  href="<c:url value="/findproduct" />" role="button">Find Products</a>
    <a  href="<c:url value="/ajax" />" role="button">Find Products AJAX</a>

  </sec:authorize>


</div>


<div class="container">

  <div class="jumbotron" style="margin-top: 20px;">
    <h1>Add Product</h1>

    <form action="${pageContext.request.contextPath}/addproduct" method="post">

      <input name="name" placeholder="Product Name">
      <input name="price" placeholder="Price">
      <input type="submit" value="Submit">
    </form>


  </div>


</div>
</body>
</html>
