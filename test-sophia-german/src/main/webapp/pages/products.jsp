<%--
  Created by IntelliJ IDEA.
  User: Sofiko
  Date: 01.04.15
  Time: 20:48
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>PRODUCT LIST</title>

  <!-- Bootstrap core CSS -->
  <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">

  <%--<!-- Custom styles for this template -->--%>
  <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">


</head>
<body>

<div class="jumbotron" style="margin-top: 20px;">
    <h2>MENU</h2>

    <sec:authorize access="isAuthenticated()">

        <a  href="<c:url value="/addproduct" />" role="button">Add Product</a>
        <a  href="<c:url value="/findproduct" />" role="button">Find Products</a>

    </sec:authorize>


</div>




<div class="jumbotron" style="margin-top: 20px;">
      <table class="table" border="2" align="center" cellspacing="5">
            <tr class="table-row-cell">
                <td class="table-view ">NAME</td>
                <td class="table-view ">PRICE</td>
            </tr>

            <c:forEach var="products" items="${products}">
           <tr class="table-row-cell">
                <td class="table-view ">${products.name}</td>
                <td class="table-view ">${products.price}</td>
            </tr>
            </c:forEach>

      </table>

</div>
</body>
</html>
