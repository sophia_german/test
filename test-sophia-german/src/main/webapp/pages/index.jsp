<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>HOME PAGE</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">

    <%--<!-- Custom styles for this template -->--%>
    <link href="<c:url value="/pages/css/signing.css" />" rel="stylesheet">


</head>

<body>

<div class="jumbotron" style="margin-top: 20px;">
    <h2>MENU</h2>

        <sec:authorize access="isAuthenticated()">

             <a  href="<c:url value="/products" />" role="button">Go To Products</a>
             <a  href="<c:url value="/addproduct" />" role="button">Add Product</a>
             <a  href="<c:url value="/findproduct" />" role="button">Find Products</a>
            <a  href="<c:url value="/ajax" />" role="button">Find Products AJAX</a>

        </sec:authorize>


</div>


<div class="container">

    <div class="jumbotron" style="margin-top: 20px;">
        <h1>Login Page</h1>

        <sec:authorize access="!isAuthenticated()">
            <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Login</a></p>
        </sec:authorize>

        <sec:authorize access="isAuthenticated()">

            <p>Your Username:  <sec:authentication property="principal.username" /></p>
            <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">LogOut</a></p>

        </sec:authorize>

    </div>


</div>
</body>
</html>
