<%--
  Created by IntelliJ IDEA.
  User: Sofiko
  Date: 06.04.15
  Time: 19:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
  <title></title>

  <title>SEARCH Ajax</title>


  <link href="<c:url value="/pages/css/bootstrap.min.css" />" rel="stylesheet">


  <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">

  <script type="text/JavaScript" src="${pageContext.request.contextPath}/pages/js/jquery-1.9.1.min.js">
  </script>
  <script>

    function doSearch() {
      $.getJSON("${pageContext.request.contextPath}/ajaxSearch", { CHARS: $('#searchBox').val() }, function(data)
      {
        $('#results').text('');

        for (var index in data) {
          $('#results').append('<p>'+'NAME --- ' + data[index].name +'   '+'PRICE ---  '+ data[index].price + '</p>');
        }
      });
    }

  </script>


</head>
<body>

<div class="jumbotron" style="margin-top: 20px;">
  <h2>MENU</h2>

  <sec:authorize access="isAuthenticated()">

    <a  href="<c:url value="/addproduct" />" role="button">Add Product</a>
    <a  href="<c:url value='/findproduct' />" role="button">Find Products</a>
    <a  href="<c:url value="/products" />" role="button">Go To Products</a>

  </sec:authorize>


</div>


<div class="jumbotron" style="margin-top: 20px;">

  <h1>Search by name ajax</h1>


  <input id="searchBox" name="name" onKeyUp="doSearch();">


  <div id="results">

  </div>



</div>

</body>
</html>

